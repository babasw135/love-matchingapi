package com.example.lovematchingapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LoveMatchingapiApplication {

    public static void main(String[] args) {
        SpringApplication.run(LoveMatchingapiApplication.class, args);
    }

}
