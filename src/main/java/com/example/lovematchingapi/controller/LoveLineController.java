package com.example.lovematchingapi.controller;

import com.example.lovematchingapi.Entity.Member;
import com.example.lovematchingapi.Model.LoveLine.LoveLineCreateRequest;
import com.example.lovematchingapi.service.LoveLineService;
import com.example.lovematchingapi.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/love-lien")
public class LoveLineController {
    private final MemberService memberService;
    private final LoveLineService loveLineService;

    @PostMapping("/new/member-id/{memberId}")
    public String setLoveLine(@PathVariable long memberId, @RequestBody LoveLineCreateRequest request){
        Member member = memberService.getData(memberId);
        loveLineService.setLoveLine(member, request);

        return "ok";
    }
}
