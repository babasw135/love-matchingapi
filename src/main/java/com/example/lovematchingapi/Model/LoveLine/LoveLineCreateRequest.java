package com.example.lovematchingapi.Model.LoveLine;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoveLineCreateRequest {
    private String lovePhoneNumber;
}
