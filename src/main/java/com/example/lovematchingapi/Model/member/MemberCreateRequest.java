package com.example.lovematchingapi.Model.member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberCreateRequest {
    private String name;
    private String phoneNumber;
    private Boolean isMan;
}
