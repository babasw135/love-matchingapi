package com.example.lovematchingapi.Entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter

public class LoveLine {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberid",nullable = false)
    private Member member;

    @Column(nullable = false, length = 13)
    private String lovePhoneNumber;

}
