package com.example.lovematchingapi.service;

import com.example.lovematchingapi.Entity.LoveLine;
import com.example.lovematchingapi.Entity.Member;
import com.example.lovematchingapi.Model.LoveLine.LoveLineCreateRequest;
import com.example.lovematchingapi.repository.LoveLineRepository;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LoveLineService {
    private final LoveLineRepository loveLineRepository;

    public void setLoveLine(Member member, LoveLineCreateRequest request){
        LoveLine addData = new LoveLine();
        addData.setMember(member);
        addData.setLovePhoneNumber(request.getLovePhoneNumber());

        loveLineRepository.save(addData);
    }
}
