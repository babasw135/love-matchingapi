package com.example.lovematchingapi.repository;

import com.example.lovematchingapi.Entity.LoveLine;
import com.example.lovematchingapi.Entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LoveLineRepository extends JpaRepository<LoveLine, Long> {
}
