package com.example.lovematchingapi.repository;

import com.example.lovematchingapi.Entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<Member, Long> {
}
